# Design Trends

2022

```
https://www.niarratravel.com/
https://crustac.fr/en/home/
https://seed.com/
https://www.hyperframe.com/
https://singita.com/
https://miti-navi.com/fr
```

Beaver Design Inspirations

https://www.ultimatebeaver.com/
https://www.ultimatebeaver.com/beaver-builder-templates/

```
https://storygatherings.com/ - Callout, Infobox, HoverBox
https://www.columbiacorp.com/ - Highlight Box
https://www.erinflynn.com/ - Header and Footer Layout
https://www.1daywebs.com/ - Header Layout, Menu Popup, Heading (zoom effect), Photo (zoom effect) zed-index, infobox
https://louderagency.com/ - banner video layout, hoverbox, strip box, blurbs
https://digiden.cm/ - row shapes, instagram feed, gallery
https://www.fullsteam.com.au/ - ANIMATION, ICONS
https://datadrivenlabs.io/ - uabb fancy text, uabb flip box, infobox, row shape separator, col box border
https://jessieonajourney.com/ - header layout, section strip (brush effect)
https://www.outlawsburgerbarn.com/ - animations
https://mathefrosch.com/ - navigation, design layout
https://transforminteractive.com/ - header layout, dynamic text
https://shotkit.com/ - header layout
https://www.newsprout.com.au/ - speedtest iframe, search form layout
https://fullrotation.com/ - animation, icons, column box border, info list, uabb divider, info box
https://worn.nyc/ - column hover, menu popup, marquee
https://walkinbackrub.co.uk/ - rev slider, footer
https://segeriusbrucecoaching.com/ - instagram feed, column layout
https://body-licious.co.za/ - footer layout, search form design
https://pixelhappy.co/ - row layouts, testimonials
https://vortexrecipes.com/ - design layout
https://lifterlms.com/ - design layout
https://ouragingworld.com/ - design layout
https://www.hi-chew.com/ - design layout
```


```
https://templates.ultimatebeaver.com/landing/offers/ - infobox (dual heading), infobox (heading) w seperator, infobox (icon)
https://templates.ultimatebeaver.com/landing/lead-magnet/ - infobox, image overlap, design layout
https://templates.ultimatebeaver.com/landing/online-portal - infobox, design layout
https://templates.ultimatebeaver.com/landing/product-release/ - infobox, design layout
https://templates.ultimatebeaver.com/landing/multipurpose/ - infobox, design layout
https://templates.ultimatebeaver.com/landing/online-course/ - infobox, design layout, countdown timer
https://templates.ultimatebeaver.com/landing/portfolio/ - infobox, uabb photo gallery, advanced icon, uabb contact form
https://templates.ultimatebeaver.com/landing/coming-soon/ - infobox, design layout
https://templates.ultimatebeaver.com/landing/event-registration/ - infobox, design layout
https://templates.ultimatebeaver.com/landing/software/ infobox, design layout
https://templates.ultimatebeaver.com/landing/app/ - infobox, design layout, border img
https://templates.ultimatebeaver.com/landing/wedding/ - infobox, uabb heading
https://templates.ultimatebeaver.com/landing/creative-agency/ -infobox, pricing, info list icon (testimonials), accordion
https://templates.ultimatebeaver.com/landing/event - infobox, info list
https://templates.ultimatebeaver.com/landing/service/ - infobox, design layout
https://templates.ultimatebeaver.com/landing/multipurpose-2/ - infobox, uabb heading
https://templates.ultimatebeaver.com/landing/coming-soon-2/ - uabb subscribe form, design layout
**https://templates.ultimatebeaver.com/landing/e-learning/** - infobox, modal, uabb photo, pricing, design layout, uabb slide box, uabb number
https://templates.ultimatebeaver.com/landing/blog/ - infobox, list icon, design layout
https://templates.ultimatebeaver.com/landing/beauty-parlour/ - design layout, infobox
https://templates.ultimatebeaver.com/landing/book-store/ - infobox, design layout, list icon, uabb testimonials, accordion
https://templates.ultimatebeaver.com/landing/product-trial/ - design layout, infobox
https://templates.ultimatebeaver.com/landing/online-training - design layout
https://templates.ultimatebeaver.com/landing/introduce-product - design layout
https://templates.ultimatebeaver.com/landing/financial-services/ - infobox, design layout
https://templates.ultimatebeaver.com/landing/registration/ - infobox, design layout
https://templates.ultimatebeaver.com/landing/product - infobox, design layout, accordion
https://templates.ultimatebeaver.com/landing/marketing/ - infobox, design layout
https://templates.ultimatebeaver.com/landing/charity/ - infobox, team, **uabb blog posts**, flip box
**https://templates.ultimatebeaver.com/landing/tourism/** - infobox, design layout
https://templates.ultimatebeaver.com/landing/spa/ - infobox, design layout, uabb number
https://templates.ultimatebeaver.com/landing/seminar/ - infobox, design layout
https://templates.ultimatebeaver.com/landing/beauty-treatment - infobox, design layout
https://templates.ultimatebeaver.com/landing/conference/ - col background, infobox, design layout
https://templates.ultimatebeaver.com/landing/web-agency/ - uabb fancy heading, infobox, design layout, uabb number
https://templates.ultimatebeaver.com/logistics/ - design layout, infobox, info list icon
**https://templates.ultimatebeaver.com/grooming-styling/** - design layout, infobox w/ border, uabb masonry, team, col border left/right
https://templates.ultimatebeaver.com/landing/agency-service/ - design layout, infobox, progress box
**https://templates.ultimatebeaver.com/landing/travels/** - infobox, design layout, info banner, dual color heading
```





Showcase BB
```
https://probeaver.com/beaver-showcase/
https://beaverbunch.com/collection/
```


Beaver Links
```
https://beaver.wpxpro.com/modules/hover-card/
https://wpbeaveraddons.com/demo/hover-cards/
https://docs.wpbeaverbuilder.com/beaver-builder/layouts/modules/callout-and-call-to-action/
https://wpbeaveraddons.com/demo/info-box/
**https://www.ultimatebeaver.com/modules/info-box/**
```


